package main

import (
	"gitlab.com/nomapunk/soa-hmw-2/internal/adapter/grpc_server"
	"gitlab.com/nomapunk/soa-hmw-2/internal/proto"
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"net"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	s := grpc.NewServer()
	srv := grpc_server.NewServer()
	proto.RegisterMafiaServiceServer(s, srv)
	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Start server")
	if err := s.Serve(l); err != nil {
		log.Fatal(err)
	}
}
