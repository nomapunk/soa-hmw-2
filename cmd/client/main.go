package main

import (
	"context"
	"fmt"
	"gitlab.com/nomapunk/soa-hmw-2/internal/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"hash/fnv"
	"io"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func FNV32a(text string) uint32 {
	algorithm := fnv.New32a()
	algorithm.Write([]byte(text))
	return algorithm.Sum32()
}

func main() {
	rand.Seed(time.Now().UnixNano() + int64(FNV32a(os.Getenv("HOSTNAME"))))
	user := os.Getenv("USER")
	role := os.Getenv("ROLE")
	if role == "bot" {
		user += fmt.Sprintf("%d", rand.Int31())
	}
	addr := ":8080"
	if len(os.Getenv("HOSTNAME")) > 0 {
		addr = "server" + addr
	}
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal(err)
	}
	client := proto.NewMafiaServiceClient(conn)
	stream, err := client.Join(context.Background(), &proto.Player{Name: user})
	if err != nil {
		log.Fatal(err)
	}
	done := make(chan bool)
	go func() {
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				done <- true
				log.Fatal(err)
			}
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", resp.Text)
			for i := 0; ; i++ {
				if resp.Type == proto.EventEnum_MUST_ANSWER {
					var input string
					players, err := client.GetPlayersList(context.Background(),
						&proto.Room{
							RoomId: resp.RoomId,
						})
					if i != 0 {
						if role != "bot" {
							fmt.Scanln(&input)
						} else {
							input = fmt.Sprintf("%d", rand.Int31()%int32(len(players.Players)))
						}
					}
					if input == "GetPlayers" || i == 0 {
						if err != nil {
							log.Fatal(err)
						}
						for _, value := range players.Players {
							fmt.Println(value.Name)
						}
						continue
					}
					val, err := strconv.Atoi(input)
					if err != nil || val < 0 {
						log.Fatal(err)
					}
					resp.Response = new(proto.Vote)
					resp.Response.To = int32(val)
					client.ResponseEvent(context.Background(), resp)
				}
				break
			}
		}
	}()
	<-done
	log.Printf("Finish")
}
