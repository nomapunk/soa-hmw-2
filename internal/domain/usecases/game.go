package usecases

import (
	"fmt"
	"gitlab.com/nomapunk/soa-hmw-2/internal/proto"
	"log"
	"sync"
)

type Game struct {
	room    *Room
	game    *proto.Game
	wg      sync.WaitGroup
	mtx     sync.Mutex
	clients []*GameClient
}

func (g *Game) SendSelectPlayers(event *proto.Event, selector func(int) bool, wait bool) {
	cnt := 0
	for i := 0; i < len(g.clients); i++ {
		if selector(i) {
			cnt += 1
		}
	}
	if wait {
		g.mtx.Lock()
		g.wg.Add(cnt)
		g.game.Votes = make([]*proto.Vote, 0)
		g.mtx.Unlock()
	}
	for i := 0; i < len(g.clients); i++ {
		if selector(i) {
			log.Println(event.Text, g.clients[i].game.room.room.Players[i].Name)
			text := "Hello, " + g.clients[i].game.room.room.Players[i].Name + " " + event.Text
			if event.Type == proto.EventEnum_NIGHT_EVENT {
				text = text + fmt.Sprintf("\nCharacter: %s",
					g.clients[i].game.game.CurrentPlayers[i].Character.String())
			}
			if event.Type == proto.EventEnum_MUST_ANSWER {
				text = text + "\nEnter GetPlayers for get list players"
			}
			currentEvent := &proto.Event{
				Text:     text,
				Type:     event.Type,
				Response: event.Response,
				RoomId:   g.room.room.RoomId,
				UserId:   event.UserId,
			}
			err := g.clients[i].srv.Send(currentEvent)
			if err != nil {
				log.Fatal(err)
				return
			}
		}
	}
	if wait {
		g.wg.Wait()
	}
}

func (g *Game) GetMaxIndex() int {
	cnt := make([]int, len(g.clients))
	maxIndex := 0
	for _, vote := range g.game.Votes {
		cnt[vote.To]++
		if cnt[maxIndex] < cnt[vote.To] {
			maxIndex = int(vote.To)
		}
	}
	return maxIndex
}

func (g *Game) Run() {
	defer func() {
		for i := 0; i < len(g.game.CurrentPlayers); i++ {
			g.room.done <- true
		}
	}()
	for {
		mafiaAlive := false
		cntNotMafia := 0
		for index, _ := range g.clients {
			log.Println(index, g.game.CurrentPlayers[index].Player.Name, g.game.CurrentPlayers[index].Character.String())
			player := g.game.CurrentPlayers[index]
			if !player.Alive {
				continue
			}
			if player.Character == proto.Character_MAFIA {
				mafiaAlive = true
			} else {
				cntNotMafia++
			}
		}
		log.Printf("ROOM %d, cntNotMafia = %d", g.room.room.RoomId, cntNotMafia)
		if !mafiaAlive {
			g.SendSelectPlayers(
				&proto.Event{
					Text: "MAFIA LOSE!",
				},
				func(i int) bool {
					return true
				},
				false,
			)
			return
		}
		if cntNotMafia < 2 {
			g.SendSelectPlayers(
				&proto.Event{
					Text: "MAFIA WIN",
				},
				func(i int) bool {
					return true
				},
				false,
			)
			return
		}
		g.SendSelectPlayers(
			&proto.Event{
				Text: "Night...",
				Type: proto.EventEnum_NIGHT_EVENT,
			},
			func(i int) bool {
				return true
			},
			false,
		)
		protectID := -1
		g.SendSelectPlayers(
			&proto.Event{
				Text:   "Medic protect...",
				RoomId: g.room.room.RoomId,
				Type:   proto.EventEnum_MUST_ANSWER,
			},
			func(i int) bool {
				player := g.game.CurrentPlayers[i]
				return player.Alive && player.Character == proto.Character_MEDIC
			},
			true,
		)
		protectID = g.GetMaxIndex()
		log.Printf("Try get protectID: %d heal %s", protectID, g.game.CurrentPlayers[protectID].Player.Name)
		g.SendSelectPlayers(
			&proto.Event{
				Text:   "Mafia kill...",
				RoomId: g.room.room.RoomId,
				Type:   proto.EventEnum_MUST_ANSWER,
			},
			func(i int) bool {
				player := g.game.CurrentPlayers[i]
				return player.Alive && player.Character == proto.Character_MAFIA
			},
			true,
		)
		maxIndex := g.GetMaxIndex()
		log.Printf("Try get killedID: %d kill %s", maxIndex, g.game.CurrentPlayers[maxIndex].Player.Name)
		if maxIndex == protectID {
			g.SendSelectPlayers(
				&proto.Event{
					Text: "All right...all people alive",
				},
				func(i int) bool {
					return true
				},
				false,
			)
		} else {
			g.mtx.Lock()
			g.game.CurrentPlayers[maxIndex].Alive = false
			g.mtx.Unlock()
			g.SendSelectPlayers(
				&proto.Event{
					Text: fmt.Sprintf("User %s has been killed", g.game.CurrentPlayers[maxIndex].Player.Name),
				},
				func(i int) bool {
					return true
				},
				false,
			)
		}
		g.SendSelectPlayers(
			&proto.Event{
				Text: "Time to vote...",
				Type: proto.EventEnum_MUST_ANSWER,
			},
			func(i int) bool {
				return g.game.CurrentPlayers[i].Alive
			},
			true,
		)
		maxIndex = g.GetMaxIndex()
		g.SendSelectPlayers(
			&proto.Event{
				Text: fmt.Sprintf(
					"User %s has been kicked and his role: %s",
					g.game.CurrentPlayers[maxIndex].Player.Name,
					g.game.CurrentPlayers[maxIndex].Character.String()),
			},
			func(i int) bool {
				return true
			},
			false,
		)
		g.mtx.Lock()
		g.game.CurrentPlayers[maxIndex].Alive = false
		g.mtx.Unlock()
	}
}

type GameClient struct {
	game *Game
	srv  proto.MafiaService_JoinServer
}
