package usecases

import (
	"fmt"
	"gitlab.com/nomapunk/soa-hmw-2/internal/proto"
	"log"
	"math/rand"
	"sync"
)

type Room struct {
	room *proto.Room
	g    *Game
	mtx  sync.Mutex
	wg   sync.WaitGroup
	done chan bool
}

func (r *Room) GenerateIntoPlayers(players []*proto.Player) []*proto.IntoPlayer {
	result := make([]*proto.IntoPlayer, len(players))
	characters := make([]proto.Character, 0)
	characters = append(characters, proto.Character_MAFIA)
	characters = append(characters, proto.Character_MEDIC)
	for i := 2; i < len(players); i++ {
		characters = append(characters, proto.Character_CITIZEN)
	}
	rand.Shuffle(len(characters), func(i, j int) {
		characters[i], characters[j] = characters[j], characters[i]
	})
	for i := 0; i < len(players); i++ {
		result[i] = &proto.IntoPlayer{
			Player:    players[i],
			Character: characters[i],
			Alive:     true,
		}
	}
	return result
}

func (r *Room) Join(player *proto.Player, srv proto.MafiaService_JoinServer) error {
	err := func() error {
		r.mtx.Lock()
		defer r.mtx.Unlock()
		if int32(len(r.room.Players)) == r.room.MaxPlayers {
			return fmt.Errorf("room %d overflow", r.room.RoomId)
		}
		if int32(len(r.room.Players)) == 0 {
			r.createGame()
			r.wg.Add(4)
			r.room.Players = make([]*proto.Player, 0)
		}
		r.room.Players = append(r.room.Players, player)
		log.Printf("New player in room %d (%d/%d), hello %s",
			r.room.RoomId,
			len(r.room.Players),
			r.room.MaxPlayers,
			player.Name)
		r.g.mtx.Lock()
		r.g.clients = append(r.g.clients, &GameClient{
			game: r.g,
			srv:  srv,
		})
		r.g.mtx.Unlock()
		if int32(len(r.room.Players)) == r.room.MaxPlayers {
			r.room.Game.CurrentPlayers = r.GenerateIntoPlayers(r.room.Players)
			go func() {
				r.g.Run()
			}()
		}
		return nil
	}()
	if err != nil {
		return err
	}
	r.wg.Done()
	log.Printf("Player %s in room %d wait start game...", player.Name, r.room.RoomId)
	r.wg.Wait()
	return nil
}

func (r *Room) AddVote(vote *proto.Vote) {
	r.g.mtx.Lock()
	r.room.Game.Votes = append(r.room.Game.Votes, vote)
	r.g.wg.Done()
	r.g.mtx.Unlock()
}

func (r *Room) createGame() {
	r.g = &Game{
		room:    r,
		game:    r.room.Game,
		wg:      sync.WaitGroup{},
		mtx:     sync.Mutex{},
		clients: make([]*GameClient, 0),
	}
}
