package usecases

import (
	"fmt"
	"gitlab.com/nomapunk/soa-hmw-2/internal/proto"
	"log"
	"os"
	"strconv"
	"sync"
)

type Server struct {
	mtx         sync.Mutex
	rooms       map[int32]*Room
	roomCounter int32
	maxPlayers  int32
}

func (s *Server) GetPlayersList(room *proto.Room) (*proto.Players, error) {
	value, ok := s.rooms[room.RoomId]
	if !ok {
		return nil, fmt.Errorf("Not found %d", room.RoomId)
	}
	resultSlice := make([]*proto.Player, 0)
	for index, key := range value.room.Game.CurrentPlayers {
		if key.Alive {
			resultSlice = append(resultSlice, &proto.Player{Name: key.Player.Name + fmt.Sprintf(" (%d)", index)})
		}
	}
	result := &proto.Players{
		Players: resultSlice,
	}
	return result, nil
}

func NewServer() *Server {
	maxPlayers, _ := strconv.ParseInt(os.Getenv("MAX_PLAYERS"), 10, 32)
	return &Server{
		mtx:         sync.Mutex{},
		rooms:       make(map[int32]*Room, 0),
		roomCounter: 0,
		maxPlayers:  int32(maxPlayers),
	}
}

func (s *Server) CreateRoom() int32 {
	var room = new(Room)
	var game = new(proto.Game)
	game = &proto.Game{
		CurrentPlayers: make([]*proto.IntoPlayer, 0),
		Votes:          make([]*proto.Vote, 0),
	}
	room.room = &proto.Room{
		Game:       game,
		RoomId:     s.roomCounter,
		MaxPlayers: s.maxPlayers,
	}
	room.done = make(chan bool)
	s.rooms[s.roomCounter] = room
	s.roomCounter += 1
	return s.roomCounter - 1
}

func (s *Server) getFreeRoomID() int32 {
	for key, element := range s.rooms {
		if len(element.room.Players) < int(element.room.MaxPlayers) {
			return key
		}
	}
	return s.CreateRoom()
}

func (s *Server) getFreeRoom() *Room {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	return s.rooms[s.getFreeRoomID()]
}

func (s *Server) Join(in *proto.Player, srv proto.MafiaService_JoinServer) error {
	room := s.getFreeRoom()
	if err := room.Join(in, srv); err != nil {
		log.Fatal(err)
		return err
	}
	<-room.done
	return nil
}

func (s *Server) ResponseEvent(event *proto.Event) {
	room := s.rooms[event.RoomId]
	room.AddVote(event.Response)
}
